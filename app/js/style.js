$( document ).ready(function() {

    $('tr[data-href]').on("click", function() {
        document.location = $(this).data('href');
        return false;
    });

    $("input[type='tel']").mask("+7 999 999-99-99");

    /*Service*/
    $('.group--pay').delegate(".add-service","click", function() {
        let $this = $(this);
        let $el = $this.closest('.online-pay');

        $this.removeClass('btn--blue add-service').addClass('btn--red remove-service').text('Удалить');

        $el.after(`
            <div class="group__inputs online-pay flex">
                <div>
                    <label class="input--group">
                        <span class="input__title">Услуга</span>
                        <select class="input input--aero" name="">
                            <option value="" selected>Услуга</option>
                            <option value="">Услуга 2</option>
                            <option value="">Услуга 3</option>
                            <option value="">Услуга 4</option>
                        </select>
                    </label>
                </div>
                <div>
                    <label class="input--group">
                        <span class="input__title">Количество</span>
                        <input type="text" class="input">
                    </label>
                </div>
                <div>
                    <div class="input--group">
                        <span class="input__title">Стоимость (руб.)</span>
                        <input type="number" class="input">
                    </div>
                </div>
                <div>
                    <div class="btn--wrap">
                        <div class="btn btn--blue add-service">добавить еще</div>
                    </div>
                </div>
            </div>
        `);
    });
    $('.group--pay').delegate(".remove-service","click", function() {
        $(this).closest('.online-pay').remove();
    });

    /*loadSvg*/
    $('[data-svg]').each(function(){
        var $this = $(this);
        var $svg = $this.data('svg');
        var $filename = $svg.split('\\').pop().split('/').pop().replace(".svg", "");

        $this.load($svg, function(responseTxt, statusTxt){
            if(statusTxt == "success"){
                $this.find('svg').addClass('svg svg-'+$filename+'');
            }
        });
    });
    thumbImg();
    /*menu*/
    $('.header__toggle-menu').on('click', function () {
        var $this = $(this);
        $this.closest('.header').toggleClass('menu-open');
    });

});

function thumbImg() {
    $('[data-thumb]').each(function () {
        var $this = $(this);
        var img = $this.find('img').attr('src');
        var size = $this.data('thumb');
        $this.css({
            'background-image': 'url(' + img + ')',
            'background-size': '' + size + ''
        });
    });
    return false;
}
